import java.util.ArrayList;
import java.util.Arrays;

public class Anagrams {

  public static ArrayList<String> runAnagrams(String inputString) {
    String[] inputArray = inputString.split(" ");
    ArrayList<Character[]> arrayOfCharArrays = new ArrayList<Character[]>();
    ArrayList<String> originalWords = new ArrayList<String>();
    ArrayList<String> outputWords = new ArrayList<String>();

    for (Integer i = 0; i < inputArray.length; i++) {
      originalWords.add(inputArray[i]);
    }
    for (Integer i = 0; i < inputArray.length; i++) {
      arrayOfCharArrays.add(Arrays.asList(inputArray[i].toCharArray()));
      Arrays.sort(arrayOfCharArrays[i]);
    }
    for (Integer i = 0; i < arrayOfCharArrays.length; i++) {
      for (Integer j = 0; j < arrayOfCharArrays.length ;j++ ) {
        if (i != j && arrayOfCharArrays[i].equals(arrayOfCharArrays[j])) {
          outputWords.add(originalWords[i]);
        }

      }
    }
    return outputWords;
  }
}
